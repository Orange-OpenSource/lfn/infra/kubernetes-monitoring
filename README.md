
# :warning: Deprecation notice  :warning:

This is no longer maintained, please use the role instead
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes-monitoring-role/

# Kubernetes Monitoring

Ansible role that configure monitoring for infra of a Kubernetes.

you'll need a playbook and an inventory to launch it.

the inventory needs a `kube-master` group where `helm` binary is present.
For now, you also need a description of your platform, following "OPNFV
standard" (PDF file) but this shouldn't be the case in the future.

then you can launch it: `ansible-playbook -i inventory playbook.yml`.

Grafana will have some preprovisionned dashboards that are known to be
interested for Kubernetes cluster monitoring.

## Inventory example

```Ansible
[kube-master]
control01
[kube-node]
compute01
compute02
compute03
compute04
compute05
compute06
compute07
compute08
compute09
compute10
compute11
compute12
[etcd]
control01

[k8s-cluster:children]
kube-master
kube-node
```

## Playbook example

```YAML
---
- hosts: k8s-cluster
  vars_files:
    - "vars/pdf.yml"
  roles:
    - role: kubernetes-monitoring
```


## TODO

- [ ] add more dashboards for infrastructure supervision
- [ ] configure kubernetes prometheus service endpoints
- [ ] create some alerts
